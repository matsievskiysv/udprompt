%udprompt

*There's a [TCPrompt](https://gitlab.com/matsievskiysv/tcprompt) alternative. Consider using it instead.*

# UDPrompt #

*UDPrompt* stands for *UDP prompt*. This is a simple prompt for [awesomewm](https://awesomewm.org/) session. It allows user to connect to running awesome Lua session and examine (and probably mutate) global variables.

## Configuration ##

Configuration of *UDPrompt* is very simple.

Clone repository to you awesome configuration folder
```bash
> git clone https://gitlab.com/matsievskiysv/udprompt ~/.config/aweseomewm/udprompt
```
or add git submodule if you use version control for your settings
```bash
> git submodule add https://gitlab.com/matsievskiysv/udprompt
```

Add one line you `rc.lua` file
```lua
require("udprompt")(9999)
```

Substitute `9999` with any other UDP port you want *UDPrompt* to listen to.

## Running UDPrompt ##

You may connect to *UDPrompt* using [netcat](https://nc110.sourceforge.io/) tool (It may be called `nc`, `ncat` or `netcat` on your system).
The command is
```bash
> ncat -u 127.0.0.1 9999
```
Now you may type code more or less like you would in Lua REPL. You can define functions
```lua
function test(a, b, c) return c, b, a end
```
you can run them and print output
```lua
print(test(1, 2, 3))
```
you can use `=()`shorthand to wrap your code in `return()` function
```lua
R=2.2
=math.pi*R^2
```
you can access awesome data structures
```lua
=awesome.release
```
you can even spawn programs
```lua
awesome.spawn("xterm")
```
*UDPrompt* operates in global environment, so you only have access to globally defined variables. You can print list of global variables like this
```lua
for i,j in pairs(_G) do print(i, j) end
```

## Limitations ##

- *UDPrompt* uses UDP protocol which is connectionless, so it **only accepts one-liners**.
- *UDPrompt* only has access to global environment. If you want to examine some variable in your code, make sure that it is global.
- *UDPrompt* accepts connections only from `localhost` by default. If you really want to connect from elsewhere, pass `"0.0.0.0"` as a second argument to initialization function.
