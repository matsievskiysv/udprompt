local cbuffer = {}
cbuffer.__index = cbuffer

setmetatable(cbuffer, {
		__call = function (cls, ...)
		   return cls.new(...)
		end,
})

function cbuffer.new(N)
   assert(N>1, "Buffer too short!")
   local self = setmetatable({}, cbuffer)
   self.size = N
   self.next = 1
   self.buffer = {}
   return self
end

function cbuffer:push(val)
   assert(type(self) == "table", "Wrong `self' type. Maybe you forgot to use colon in buffer:push()?")
   self.buffer[self.next] = val
   if self.next == self.size then
      self.next = 1
   else
      self.next = self.next + 1
   end
end

function cbuffer:set(val)
   assert(type(self) == "table", "Wrong `self' type. Maybe you forgot to use colon in buffer:set()?")
   self.buffer[self.next] = val
end

function cbuffer:clear()
   assert(type(self) == "table", "Wrong `self' type. Maybe you forgot to use colon in buffer:clear()?")
   self.next = 1
   self.buffer = {}
end

function cbuffer:iterate()
   assert(type(self) == "table", "Wrong `self' type. Maybe you forgot to use colon in buffer:iterate()?")
   local i = 0
   local j = self.next
   return function()
      local p
      repeat
         -- find next non-nil value
         i = i + 1
         p = j
         if j == self.size then
            j = 1
         else
            j = j + 1
         end
      until i >= self.size or self.buffer[p] ~= nil
      if i <= self.size then
         return self.buffer[p]
      end
   end
end

return cbuffer
