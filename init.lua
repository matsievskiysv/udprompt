local lgi = require "lgi"
local Gio = lgi.Gio
local GLib = lgi.GLib
local core = require "lgi.core"
local assert = lgi.assert
local CBuffer = require "udprompt.cbuffer"

local print_buffer = CBuffer(1000)

local lenv = {}
setmetatable(lenv, {__index=_G})

-- overwrite global print function
function lenv.print(...)
   local args = table.pack(...)
   local line = string.format("%s", args[1])
   for i=2,args.n do
      line = string.format("%s\t%s", line, args[i])
   end
   print_buffer:push(line)
end

local function new(port, address)
   -- https://github.com/pavouk/lgi/blob/master/samples/udpsrv.lua
   local socket = Gio.Socket.new(Gio.SocketFamily.IPV4,
				 Gio.SocketType.DATAGRAM,
				 Gio.SocketProtocol.UDP)
   local sa = Gio.InetSocketAddress.new(
      Gio.InetAddress.new_from_string(address or "127.0.0.1"), port)
   assert(socket:bind(sa, true))

   local source = assert(socket:create_source(
			    GLib.IOCondition.IN))
   local canc = Gio.Cancellable()
   local buf = core.bytes.new(4096)
   source:set_callback(function()
	 local len, src = assert(socket:receive_from(buf))
	 if len > 0 then
	    local input = tostring(buf):sub(1, len)
	    if input:sub(1,1) == "=" then
	       -- =x is a shorthand for return(x)
	       input = "return(" .. input:sub(2, len-1) .. ")\n"
	    end
	    local func, err = load(input, nil, "t", lenv)
	    if err == nil then
	       local rv = table.pack(pcall(func))
	       for m in print_buffer:iterate() do
		  -- send all buffered output
		  assert(socket:send_to(src, string.format("%s\n", m), canc))
	       end
	       print_buffer:clear()
	       if rv[1] ~= true  then
		  assert(socket:send_to(src, string.format("Runtime error: %s\n", rv[2]), canc))
	       end
	       if rv[2] ~= nil then
		  local msg = rv[2]
		  for i=3,#rv do
		     msg = msg .. "\t" .. rv[i]
		  end
		  assert(socket:send_to(src, string.format("%s\n", msg), canc))
	       end
	    else
	       assert(socket:send_to(src, string.format("Parse error: %s\n", err), canc))
	    end
	 end
	 return true
   end)

   source:attach(GLib.MainContext.default())
end

return new
